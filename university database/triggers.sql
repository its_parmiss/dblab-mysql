1.

delimiter //
CREATE TRIGGER maxWageCheck BEFORE Insert ON instructor
       FOR EACH ROW
       BEGIN
           IF NEW.salary >12000 THEN
               SET NEW.salary = 12000;
           END IF;
       END;//
delimiter ;


delimiter //
CREATE TRIGGER update_tot_cred AFTER update ON student
       FOR EACH ROW
       BEGIN
           IF NEW.salary >12000 THEN
               SET	 NEW.salary = 12000;
           END IF;
       END;//
delimiter ;



2.
DELIMITER $$
CREATE TRIGGER tot_cred_update
AFTER Update ON takes FOR EACH ROW
begin
       DECLARE tot_cred int;
       DECLARE credits int;
    

       IF (new.grade != old.grade and new.grade!='F' and new.grade != null)
       THEN
		   SELECT tot_cred
		   INTO @tot_cred
		   FROM student
		   WHERE New.ID= student.ID;
           
           SELECT credits
		   INTO @credits
		   FROM course
		   WHERE New.course_id= course.course_id;
           
           UPDATE student
           SET student.tot_creds= student.tot_creds+credits
           WHERE student.ID = NEW.ID;
        END IF;
END;
$$
DELIMITER ;

3.
DELIMITER $$
CREATE TRIGGER checkNumberOfTakesCS190
Before Insert ON takes FOR EACH ROW
begin
       DECLARE NumberOfTakersSoFar int;
       DECLARE iscs190 boolean;
    

		   SELECT 1
		   INTO @iscs190
		   FROM course
		   WHERE New.course_id= course.course_id and course.title = 'CS-190';
           if iscs190 = 1 Then
			   SELECT count(*)
			   INTO @NumberOfTakersSoFar
			   FROM takes
			   WHERE New.sec_id= takes.sec_id;
			   IF NumberOfTakersSoFar >=5
			   Then  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Full';
			END IF;
        ENd If;
END;
$$
DELIMITER ;

4.

DELIMITER $$
CREATE TRIGGER checkDepartmentBeforeTake
Before Insert ON takes FOR EACH ROW
begin
       DECLARE  deptname varchar(20);
       DECLARE  studentdeptname varchar(20);

		   SELECT dept_name
		   INTO @deptname
		   FROM course
		   WHERE New.course_id= course.course_id;
           
			SELECT dept_name
		   INTO @studentdeptname
		   FROM student
		   WHERE New.ID= Student.ID;
           If deptname != studentdeptname Then
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'not same departmen';
		   END IF;
        
END;
$$




5.
DELIMITER $$
CREATE TRIGGER tot_cred_check
before insert ON takes FOR EACH ROW
begin
       DECLARE tot_cred_sem int;
       DECLARE credit int;
       
		SELECT credit
		INTO @credit
		FROM course
		WHERE New.course_id =  course.course_id;
           
		select sum(credit) into @tot_cred_sem
		from takes where new.ID = takes.ID and takes.semester = new.semester;
    
		IF ( tot_cred_sem + credit > 20 )
			then SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Credits more than 20';
		end if;
	end;

$$
DELIMITER ;

6.

DELIMITER $$
CREATE TRIGGER check_prereq
Before Insert ON takes FOR EACH ROW
begin
       DECLARE taken boolean;
    
	  SELECT 1
into taken
FROM
       (
       
           SELECT prereq_id 
           FROM   prereq
           where course_id=new.course_id
           
       ) AS t
       where t.prereq_id not in (Select course_id from takes where new.ID=takes.ID);
		if taken = 1 Then
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Full';
		END IF;

END;
$$
DELIMITER ;


