DELIMITER //
CREATE PROCEDURE students()
BEGIN
	SELECT * 
	FROM 
		student;
END //

DELIMITER ;




DELIMITER //
CREATE PROCEDURE sections_for_department(in did varchar(20))
BEGIN
	SELECT course.title , instructor.name , section.sec_id
	FROM 
		((course JOIN section ON
		course.course_id = section.course_id) JOIN teaches ON
        course.course_id = teaches.course_id ) JOIN instructor ON
        teaches.ID = instructor.ID
	WHERE
        course.dept_name = did;
END //

DELIMITER ;


DELIMITER //
CREATE PROCEDURE show_prereqs(in cid varchar(8))
BEGIN
	SELECT course.title 
    FROM 
		(SELECT prereq.prereq_id 
		FROM 
			prereq 
		WHERE
			course_id = cid) as t1
	JOIN course ON
    prereq_id  = course.course_id;
    
END //

DELIMITER ;



DELIMITER //

CREATE PROCEDURE prereqs_complete(in sid varchar(5) , in cid varchar(8) , out result varchar(3))
BEGIN
	DECLARE countPrereqs SMALLINT;
    
	SELECT 
		count(*) into countPrereqs
	FROM 
		takes
	WHERE 
		Id  = sid and NOT exists 
        ( SELECT prereq.prereq_id 
		FROM 
			prereq 
		WHERE
			course_id = cid);
	
	IF countPrereqs > 0 THEN
		SET result = 'YES';
    Else 
		SET result = 'NO';
	END IF;
    
END //

DELIMITER ;






