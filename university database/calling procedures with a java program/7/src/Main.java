import java.sql.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {

        Connection connection = null;
        try {
            connection = getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("[1] students");
        System.out.println("[2] sections for department.");
        System.out.println("[3] show prerequisites");
        System.out.println("[4] prereqs complete");


        try (Scanner in = new Scanner(System.in)) {
            int input = in.nextInt();

            switch (input) {
                case 0:
                    return;
                case 1:
                    students(connection);
                    break;
                    case 2: {
                        System.out.println("Please Enter Did");
                        String did= in.next();
                        sections_for_departments(did , connection);
                        break;
                    }
                    case 3: {
                        System.out.println("Please Enter Cid");
                        String cid= in.next();
                        show_preReqs(cid , connection);
                        break;
                    }
                    case 4: {
                        System.out.println("Please Enter Sid");
                        String sid= in.next();
                        System.out.println("Please Enter Cid");
                        String cid= in.next();

                        prereqs_complete(sid , cid , connection);
                        break;
                    }
                default:
                    System.out.println("Invalid input. Try again.");
                    menu();
                    break;
            }
        }
    }

    private static String prereqs_complete(String sid, String cid, Connection connection) {
        CallableStatement statement = null;
        try {

            statement = connection.prepareCall("{?=call show_prereqs(?)}");
            statement.setString(1, cid);
            statement.setString(2, sid);
            statement.registerOutParameter(3, Types.VARCHAR);
            boolean hadResults = statement.execute();


            String s = stmt.getString(3);
            System.out.println(s);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static void show_preReqs(String cid , Connection connection) {
        CallableStatement statement = null;

        try {
            statement = connection.prepareCall("{call show_prereqs(?)}");
            statement.setString(1, cid);
            boolean hadResults = statement.execute();

            while (hadResults) {
                ResultSet rs = statement.getResultSet();
                printResultset(rs);
                hadResults = statement.getMoreResults();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void sections_for_departments(String did , Connection connection) {
        CallableStatement statement = null;

        try {
            statement = connection.prepareCall("{call sections_for_department(?)}");
            statement.setString(1, did);
            boolean hadResults = statement.execute();

            while (hadResults) {
                ResultSet rs = statement.getResultSet();
                printResultset(rs);
                hadResults = statement.getMoreResults();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void students(Connection connection) {
        CallableStatement statement = null;
        try {
            statement = connection.prepareCall("{call students()}");
            boolean hadResults = statement.execute();

            while (hadResults) {
                ResultSet rs = statement.getResultSet();
                printResultset(rs);
                hadResults = statement.getMoreResults();

            }

            //
            // Retrieve output parameters
            //
            // Connector/J supports both index-based and
            // name-based retrieval
            //
//
//            int outputValue = statement.getInt(2); // index-based
//
//            outputValue = statement.getInt("inOutParam"); // name-based

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://localhost/university?" +
                        "user=root&password=123456&useSSL=false&allowPublicKeyRetrieval=true");

    }

    public static void printResultset(ResultSet rs) throws SQLException {

        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        while (rs.next()) {
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = rs.getString(i);
                System.out.print(columnValue + " " + rsmd.getColumnName(i));
            }
            System.out.println("");
        }
    }
}

