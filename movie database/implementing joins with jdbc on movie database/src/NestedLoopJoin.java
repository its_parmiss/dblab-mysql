import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Mohammad.Nosrati
 */
public class NestedLoopJoin extends Join {

    @Override
    protected void joinAlgorithm(Table t1, Table t2, List<String> commonColumns, Table joinResult) {
        for (Map<String, Object> t1Row : t1.getRows()) {
            for (Map<String, Object> t2Row : t2.getRows()) {
                if (rowsMatch(t1Row, t2Row, commonColumns)) {
                    joinRows(t1Row, t2Row, t1, t2, joinResult);
                }
            }
        }
    }

    protected Table leftOuterJoin(Table t1 , Table t2) {
        List<String> columns = new ArrayList<>();
        columns.addAll(t1.getColumns().stream()
                .map(col -> t1.getName() + "." + col).collect(Collectors.toList()));
        columns.addAll(t2.getColumns().stream()
                .map(col -> t2.getName() + "." + col).collect(Collectors.toList()));

        // get common columns in both tables:
        List<String> commonColumns = t1.getColumns().stream()
                .filter(c -> t2.getColumns().contains(c)).collect(Collectors.toList());
//        System.out.println("commonColumns = " + commonColumns);
        // resulting table:
        Table joinResult = new Table(t1.getName() + " |><| " + t2.getName(), columns);

        for (Map<String, Object> t1Row : t1.getRows()) {
            for (Map<String, Object> t2Row : t2.getRows()) {
                if (rowsMatch(t1Row, t2Row, commonColumns)) {
                    joinRows(t1Row, t2Row, t1, t2, joinResult);
                }else{
                    Map <String,Object> t2rownull =new HashMap<>();
                    for(String s:t1Row.keySet()){
                        t2rownull.put(s,null);
                    }
                    joinRows(t1Row,t2rownull,t1,t2,joinResult);
                }
            }
        }
        return joinResult;
    }

}
