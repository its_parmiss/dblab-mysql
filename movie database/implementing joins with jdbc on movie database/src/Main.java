import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Mohammad.Nosrati
 */
public class Main {
    public static void main(String[] args) {
        // demo();

        try (Connection connection = getConnection()) {

            Table tblDirector = directorUsingSql(connection);

            Table tblMovie = movieUsingSql(connection);
            System.out.println("joinusingsql");
            joinUsingSql(connection);

//            movieUsingSql(connection);
            System.out.println("joinusingnewtedloop");
            joinUsingNestedLoop(tblDirector, tblMovie);

            joinUsingHashJoin(tblDirector, tblMovie);

            leftOuterJoinUsingSql(connection);

            rightOuterJoinUsingSql(connection);

            leftOuterJoinUsingNestedLoop(tblDirector , tblMovie);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void rightOuterJoinUsingSql(Connection connection) {
        // TODO
    }

    private static void leftOuterJoinUsingSql(Connection connection) {
        Table tblMovie = Table.tableFromSQLQuery(connection, "SELECT * FROM movie left join director on movie.did = director.did ", "movies");
        System.out.println("leftouterjoin using sql");
        System.out.print(tblMovie);
    }

    private static void joinUsingHashJoin(Table tblDirector, Table tblMovie) {
        // TODO
    }

    private static void joinUsingNestedLoop(Table tblDirector, Table tblMovie) {
        NestedLoopJoin n = new NestedLoopJoin();
        Table result = n.join(tblDirector , tblMovie);
        System.out.println(result);
    }

    private  static void leftOuterJoinUsingNestedLoop(Table t1 , Table t2){
        NestedLoopJoin n = new NestedLoopJoin();
        Table result = n.leftOuterJoin(t1, t2);
        System.out.println("leftOuterJoinUsingNestedLoop");
        System.out.println(result);
    }

    private static void joinUsingSql(Connection connection) {
        Table tblMovie = Table.tableFromSQLQuery(connection, "SELECT * FROM movie Natural join director ", "movies");
        System.out.println(tblMovie);
    }

    private static Table movieUsingSql(Connection connection) {
        Table tblMovie = Table.tableFromSQLQuery(connection, "SELECT * FROM movie", "movies");
        System.out.println("MOVIE: ");
        System.out.println(tblMovie);
        return tblMovie;
    }

    private static Table directorUsingSql(Connection connection) {
        Table tblDir = Table.tableFromSQLQuery(connection, "SELECT * FROM director", "movies");
        System.out.println("DIRECTOR: ");
        System.out.println(tblDir);
        return tblDir;
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://localhost/movies?" +
                        "user=root&password=123456&useSSL=false&allowPublicKeyRetrieval=true");

    }
    // this is just to show you how these classes work:
    private static void demo() {
        Table t1 = new Table("T1", Arrays.asList("id", "name"));
        Table t2 = new Table("T2", Arrays.asList("id", "x"));

        for (int i = 0; i < 10; i++) {
            Map<String, Object> row = new HashMap<>();
            row.put("id", i);
            row.put("name", "name_" + new Random().nextInt(100));
            t1.getRows().add(row);
        }

        for (int i = 5; i < 15; i++) {
            Map<String, Object> row = new HashMap<>();
            row.put("id", i);
            row.put("x", "x_" + new Random().nextInt(100));
            t2.getRows().add(row);
        }

        System.out.println(t1);
        System.out.println(t2);

        Join j = new NestedLoopJoin();

        Table result = j.join(t1, t2);
        System.out.println(result.getName());
        System.out.println(result);
    }
}
