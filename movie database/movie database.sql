CREATE SCHEMA `movies` ;

create table actor(
   aid INT NOT NULL ,
   name VARCHAR(50) ,
   gender char(1),
   PRIMARY KEY ( aid )
);
create table director(
   did INT NOT NULL ,
   name VARCHAR(50) ,
   PRIMARY KEY ( did )
);

create table movie(
   mid INT NOT NULL ,
   title VARCHAR(50) ,
   year1 int,
   runtime int,
   did int,
    FOREIGN KEY (did) REFERENCES director(did),
   PRIMARY KEY ( mid )
);

create table `cast`(
   aid int,
   mid int,
    FOREIGN KEY (aid) REFERENCES actor(aid),
    FOREIGN KEY (mid) REFERENCES movie(mid)
);


INSERT INTO director
VALUES 
   (1,"Orson Welles"),
   (2,"James Stewart","M"),
   (3,"Kim Novak","F"),
 (4 , "Marlon Brando" , "M"),
(5 , "Al Pacino" , "M"),
(6, "Janet Gaynor" ,  "F"),
(7,  "Toshiro Mifune" , "M"),
(8 ,"Takashi Shimura", "M"),
(9 ,"Robert De Niro" ,"M"),
(10 ,"Jodie Foster" ,"F"),
(11, "Bibi Andersson" ,"F"),
(12, "Liv Ullmann", "F"),
(13, "Marylin Monroe" ,"F"),
(14, "Jack Lemmon", "M"),
(15, "Humphrey Bogart", "M"),
(16, "Ingrid Bergman", "F"),
(17, "Clark Gable", "M"),
(18, "Vivien Leigh", "F"),
(19, "Leonardo Di Caprio", "M"),
(20, "Kate Winslet", "F");






INSERT INTO director
VALUES 
   (1,"Orson Welles"),
  ( 2, "Alfred Hitchcock"),
(3, "Francis Ford Coppola"),
(4, "F.W. Murnau"),
(5, "Akira Kurusawa"),
(6, "Vittorio De Sica"),
(7, "Martin Scorsese"),
(8, "Ingmar Bergman"),
(9, "Billy Wilder"),
(10, "Michael Curtiz"),
(11, "James Cameron"),
(12, "Victor Fleming");

INSERT INTO movie
VALUES 
(1 ,  "Citizen Kane", 1941 , 119, 1),
(2 ,"Vertigo" ,1958 ,128 ,2),
(3 ,"The Godfather", 1972, 175, 3),
(4, "Sunrise", 1927, 94, 4),
(5 ,"The Seven Samurai", 1954 ,207 ,5),
(6 ,"Bicycle Thieves", 1948, 89 ,6),
(7 ,"Taxi Driver ",1976, 114, 7),
(8 ,"Persona ",1966 ,83 ,8),
(9 ,"Some Like it Hot " , 1959, 121, 9),
(10 ,"Casablanca" ,1942 ,102 ,10),
(11 ,"Sunset Boulevard", 1950, 110, 9),
(12 ,"Titanic", 1997 ,195 ,11),
(13,"Gone With the Wind", 1939, 221, 12);




INSERT INTO cast
VALUES 
(1, 1),
(2, 2),
(3, 2),
(4, 3),
(5, 3),
(6, 4),
(7, 5),
(8, 5),
(9, 7),
(10, 7),
(11, 8),
(12, 8),
(13, 9),
(14, 9),
(15, 10),
(16, 10),
(19, 12),
(20, 12),
(17 ,13),
(18 ,13);




select * from movie where year1>1939 & year1<=1945



select avg(runtime) from movie 


select max(runtime) from movie where year1 < 1980



select * from movie JOIN cast on cast.mid=movie.mid where aid = ( select aid from actor where name = "Humphrey Bogart")

select movie.title , director.name from movie Join director on movie.did=director.did 

select movie.title  from movie Join director on movie.did=director.did where director.name = "Billy Wilder"



select director.name from movie Join director on movie.did=director.did where movie.year1=( select min(year1) from movie)




select distinct movie.title from (movie natural Join cast) natural join actor where actor.gender = 'F'




select distinct movie.title from (movie natural Join cast) natural join actor where actor.gender = 'F' and mid not in (select distinct movie.mid from (movie natural Join cast) natural join actor where actor.gender = 'M') 






select distinct movie.title from (movie natural Join cast) natural join actor where mid not in (select distinct movie.mid from (movie natural Join cast) natural join actor where actor.gender = 'F') 

select distinct director.name from director join movie on director.did=movie.did group by director.name having count(director.name)>1
select distinct director.name from movie join director on movie.did=director.did where director.name in (select name from movie natural join director group by name having count(name)>1 )



select decade,avg(runtime) from (select movie.title, floor( (movie.year1-1900)/10) as decade,runtime from movie) as t1 group by decade








